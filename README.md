# README #

### What is this repository for? ###
This repository serves as a technical interview for developers pursuing a position at Presis.

## Instructions ##
* Your task is to build a web application that processes files containing text.
* The user should upload a file from his or her browser to the server and then be presented with the processed text.
* In the processed text the application should surround every occurrence of the most used word with foo and bar.
    * Example: if the most used word is 'presis', the processed text should display 'foopresisbar' instead of 'presis'.
* In this repository you'll find some useful textfiles for testing your application.
* Choose one of the options below for your web application:
    * Develop a web application (frontend only)
    * Develop a restful web API (backend only)
    * Develop a web application with a restful web API and frontend
* You are free to choose what specific technologies to use in this assignment, however we suggest
    * ASP.NET MVC & ASP.NET MVC Web API
    * JavaScript, AngularJS, Less
    * Microsoft SQL Server, Dapper.NET

### Handing in your assignment ###
* Bring your web application along to your interview
    * Be prepared to run and demonstrate the web application. 
    * Be prepared to present the code in an IDE of your choice on your own computer. 
* In addition to this assignment you should also be prepared to show another project you developed.

### Who do I talk to? ###
If you have any questions about your assignment you are welcome to contact Richard Pauly at [richard.pauly@presis.se](mailto:richard.pauly@presis.se)